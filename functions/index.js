const functions = require('firebase-functions')
const admin = require('firebase-admin')
const serviceAccount = require('./shareq-6df00-firebase-adminsdk-kjyo1-6710315923.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://shareq-6df00.firebaseio.com'
})

exports.createUserItem = functions.auth.user().onCreate(event => {
  let uid = event.data.uid
  let email = event.data.email
  let user = {
    email: email,
    imageUrl: '',
    name: '',
    gender: '',
    birthday: '',
    job: '',
    country: '',
    state: '',
    prefecture: '',
    groups: {
      public: true
    },
    answers: {},
    asks: {}
  }
  admin.database().ref('users/' + uid).set(user)
})
