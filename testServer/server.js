var http = require('http')
var fs = require('fs')
var querystring = require('querystring')
// var url = require('url')
var path = require('path')

var server = http.createServer(function (req, res) {
  console.log(req.method + ': ' + req.url)
  // '/'にアクセス index.html(テスト用のGET/POSTフォーム)を表示
  if (req.url === '/qs.json' && req.method === 'GET') {
    // index.htmlを読み込む
    fs.readFile(path.resolve() + '/qs.json', {
      encoding: 'utf8'
    }, function (err, html) {
      // ファイルの読み込みに失敗したらエラーのレスポンスを返す
      if (err) {
        res.statusCode = 500
        res.setHeader('Access-Control-Allow-Origin', '*')
        // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
        res.end('Error!')
      }
      // ファイルの読み込みが成功したらHTMLを返す
      else {
        res.setHeader('Content-Type', 'text/json')
        res.setHeader('Access-Control-Allow-Origin', '*')
        // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
        res.end(html)
      }
    })
  }
  // '/self.json'にアクセス かつ GETリクエストだったら
  else if (req.url === '/self.json' && req.method === 'GET') {
    // index.htmlを読み込む
    fs.readFile(path.resolve() + '/self.json', {
      encoding: 'utf8'
    }, function (err, html) {
      // ファイルの読み込みに失敗したらエラーのレスポンスを返す
      if (err) {
        res.statusCode = 500
        res.setHeader('Access-Control-Allow-Origin', '*')
        // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
        res.end('Error!')
      }
      // ファイルの読み込みが成功したらHTMLを返す
      else {
        res.setHeader('Content-Type', 'text/json')
        res.setHeader('Access-Control-Allow-Origin', '*')
        // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
        res.end(html)
      }
    })
  }
  // '/self.json/answer'にアクセス かつ POSTリクエストだったら
  else if (req.url === '/self.json/answer' && req.method === 'POST') {
    let data = ''
    // readableイベントが発火したらデータにリクエストボディのデータを追加
    req.on('readable', function (chunk) {
      data += req.read() || ''
    })
    // リクエストボディをすべて読み込んだらendイベントが発火する。
    req.on('end', function () {
      // パースする
      querystring.parse(data)
      res.setHeader('Access-Control-Allow-Origin', '*')
      // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
      res.end(data)
    })
  }
  // '/self.json/q'にアクセス かつ POSTリクエストだったら
  else if (req.url === '/self.json/q' && req.method === 'POST') {
    let data = ''
    // readableイベントが発火したらデータにリクエストボディのデータを追加
    req.on('readable', function (chunk) {
      data += req.read() || ''
    })
    // リクエストボディをすべて読み込んだらendイベントが発火する。
    req.on('end', function () {
      // パースする
      console.log(data)
      data = JSON.parse(data)
      data.q['id'] = 15
      console.log(data)
      res.setHeader('Access-Control-Allow-Origin', '*')
      // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
      res.end(JSON.stringify(data))
    })
  }
  else {
    res.statusCode = 404
    res.setHeader('Access-Control-Allow-Origin', '*')
    // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
    res.end('NotFound')
  }
})

// localhostの3000番ポートでサーバーを起動する
server.listen(8001)
console.log('server up')
