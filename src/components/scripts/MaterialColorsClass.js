export default {

  colorlabels: ['red', 'pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'cyan', 'teal', 'green', 'light-green', 'lime', 'yellow', 'amber', 'orange', 'deep-orange', 'brown', 'grey', 'blue-grey'],
  colorvalues: ['#f44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5', '#2196F3', '#03A9F4', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEB3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B'],

  getClassBindingBgColorSets (depthNum) {
    if (depthNum) {
      return this.colorlabels.map(function (value) {
        let key = 'bg-' + value + '-' + depthNum.toString()
        let dic = {}
        dic[key] = true
        return dic
      })
    }
    else {
      return this.colorlabels.map(function (value) {
        let key = 'bg-' + value
        let dic = {}
        dic[key] = true
        return dic
      })
    }
  },

  getClassBindingTextColorSets (depthNum) {
    if (depthNum) {
      return this.colorlabels.map(function (value) {
        let key = 'text-' + value + '-' + depthNum.toString()
        let dic = {}
        dic[key] = true
        return dic
      })
    }
    else {
      return this.colorlabels.map(function (value) {
        let key = 'text-' + value
        let dic = {}
        dic[key] = true
        return dic
      })
    }
  },

  getBgColorItems (depthNum) {
    if (depthNum) {
      return this.colorlabels.map(function (value) {
        let bgColor = 'bg-' + value + '-' + depthNum.toString()
        return bgColor
      })
    }
    else {
      return this.colorlabels.map(function (value) {
        let bgColor = 'bg-' + value
        return bgColor
      })
    }
  },

  getBgColorItem (index, depthNum) {
    if (depthNum) {
      return 'bg-' + this.colorlabels[index] + '-' + depthNum.toString()
    }
    else {
      return 'bg-' + this.colorlabels[index]
    }
  },

  getTextColorItems (depthNum) {
    if (depthNum) {
      return this.colorlabels.map(function (value) {
        let textColor = 'text-' + value + '-' + depthNum.toString()
        return textColor
      })
    }
    else {
      return this.colorlabels.map(function (value) {
        let textColor = 'text-' + value
        return textColor
      })
    }
  },

  getTextColorItem (index, depthNum) {
    if (depthNum) {
      return 'text-' + this.colorlabels[index] + '-' + depthNum.toString()
    }
    else {
      return 'text-' + this.colorlabels[index]
    }
  }
}
