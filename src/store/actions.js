import Vue from 'vue'
import * as types from './mutation-types'
// import * as gtypes from './getter-types'
import firebase from 'firebase'
import config from './../../firebaseconfig/config.js'

firebase.initializeApp(config)

export const logIn = ({ commit }, params) => {
  firebase.auth().signInWithEmailAndPassword(params.email, params.password).then((response) => {
    /* ログイン成功した時 */
  }, (error) => {
    /* ログイン失敗した時 */
    console.log(error.message)
  })
}

export const logOut = ({ commit }, params) => {
  firebase.auth().signOut()
}

export const signUp = ({ commit }, params) => {
  console.log(params)
  firebase.auth().createUserWithEmailAndPassword(params.email, params.password).catch((error) => {
    // Handle Errors here.
    let errorCode = error.code
    let errorMessage = error.message
    console.log(errorCode + ':' + errorMessage)
  })
}

export const postQ = ({ commit }, params) => {
  Vue.http.post('http://localhost:8001/self.json/q', { q: params.q }, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).then((response) => {
    console.log('postQ: ' + response.statusText + ': ' + response.status)
    commit(types.ADD_SELF_ASK, response.data.q.id)
    params.m.closeModal()
  }, (response) => {
    // エラー処理
    console.log(response)
  })
}

export const postAnswer = ({ commit, state }, answer) => {
  console.log(answer)
  let uid = state.self.uid
  let current = new Date().toISOString()
  firebase.database().ref('answers/' + answer.qid + '/' + uid).set({
    choice: answer.choice,
    timestamp: current
  })
  firebase.database().ref('qs/' + answer.qid + '/answeruser/' + uid).set(true)
  firebase.database().ref('users/' + uid + '/answers/' + answer.qid).set(true)
}

export const deleteQByKey = ({ state }, idx) => {
  Vue.delete(state.qs, idx)
  // state.qs.shift()
}
// export const getQs = ({ commit }) => {
//   Vue.http.get('http://localhost:8001/qs.json').then((response) => {
//     console.log('getQs: ' + response.statusText + ': ' + response.status)
//     commit(types.SET_QS, response)
//   }, (response) => {
//     // エラー処理
//     console.log(response)
//   })
// }
