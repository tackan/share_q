import * as types from './mutation-types'
import firebase from 'firebase'

export const state = {
  message: 'hello vuex',
  self: {
    uid: null,
    email: null,
    imageUrl: null,
    name: null,
    gender: null,
    birthday: null,
    job: null,
    country: null,
    state: null,
    prefecture: null,
    groups: null,
    answers: {},
    asks: {}

  },
  isLogedIn: false,
  qs: []
}

let deadlines
let keys
// ログイン時にkeysがなくてQを取得できなかった場合のflag
let KeyWasNull = false
firebase.database().ref('/deadlines').on('value', function (snapshot) {
  console.log('first')
  deadlines = snapshot.val()
  keys = Object.keys(deadlines)
  // ログイン時にkeysがなくてQを取得できなかった場合
  if (KeyWasNull) {
    let usergids = Object.keys(state.self.groups)
    let useraids = Object.keys(state.self.answers)
    console.log(keys)
    keys = keys.filter(function (value) {
      return usergids.indexOf(deadlines[value].gid) >= 0 && useraids.indexOf(value) < 0
    })
    console.log(keys)
    // とりあえず5回回る
    let count = 5
    while (keys.length > 0 && count > 0) {
      let randnum = Math.floor(Math.random() * keys.length)
      let key = keys[randnum]
      keys.splice(randnum, 1)
      firebase.database().ref('/qs/' + key).once('value').then(function (snapshot) {
        if (snapshot.val()) {
          state.qs.push({ qid: key, value: snapshot.val() })
        }
      })
      count -= 1
    }
    KeyWasNull = false
  }

  console.log(keys)
})

// 認証状態の確認
firebase.auth().onAuthStateChanged(function (user) {
  // usersItemsの変化を監視するリスナー
  let usersRef
  // 連続でプッシュされないように管理するためのflag
  let pushflag = true
  if (user) {
    // ログイン状態の処理
    state.isLogedIn = true
    state.self.uid = user.uid
    state.self.email = user.email

    usersRef = firebase.database().ref('/users/' + user.uid).on('value', function (snapshot) {
      state.self.imageUrl = snapshot.val().imageUrl // || ''
      state.self.name = snapshot.val().name // || ''
      state.self.gender = snapshot.val().gender // || ''
      state.self.birthday = snapshot.val().birthday // || ''
      state.self.job = snapshot.val().job // || ''
      state.self.country = snapshot.val().country // || ''
      state.self.state = snapshot.val().state // || ''
      state.self.prefecture = snapshot.val().prefecture // || ''
      state.self.groups = snapshot.val().groups || { public: true }
      state.self.answers = snapshot.val().answers || {}
      state.self.asks = snapshot.val().asks || {}
      if (state.qs.length < 6 && pushflag) {
        KeyWasNull = !keys
        if (KeyWasNull) {
        }
        else {
          let usergids = Object.keys(state.self.groups)
          let useraids = Object.keys(state.self.answers)
          console.log(keys)
          keys = keys.filter(function (value) {
            return usergids.indexOf(deadlines[value].gid) >= 0 && useraids.indexOf(value) < 0
          })
          console.log(keys)
          // 最大で15回回る
          let count = 10 - state.qs.length
          // 値が帰って来るまではfalseにする
          pushflag = false
          while (keys.length > 0 && count > 0) {
            let randnum = Math.floor(Math.random() * keys.length)
            let key = keys[randnum]
            keys.splice(randnum, 1)
            if (count !== 1) {
              firebase.database().ref('/qs/' + key).once('value').then(function (snapshot) {
                if (snapshot.val()) {
                  state.qs.push({ qid: key, value: snapshot.val() })
                }
              })
            }
            else {
              // カウントが 1になったときのデータ取得完了時にflagをtrueに
              firebase.database().ref('/qs/' + key).once('value').then(function (snapshot) {
                if (snapshot.val()) {
                  state.qs.push({ qid: key, value: snapshot.val() })
                }
                pushflag = true
              })
            }
            count -= 1
          }
        }
      }
      else if (state.qs.length >= 6) {
        pushflag = true
      }
      console.log(state.qs.length)
    })
  }
  else {
    // ログアウト状態の処理
    if (state.self.uid) {
      firebase.database().ref('/users/' + state.self.uid).off(usersRef)
    }
    state.isLogedIn = false
    state.self.uid = null
    state.self.email = null
    state.qs = []
  }
})

export const mutations = {
  [types.MESSAGE_UPDATE] (state, param) {
    state.message = param
  },
  [types.SET_SELF] (state, response) {
    state.self = response.data.self
  },
  [types.SET_SELF_ANSWER] (state, param) {
    state.self.answers.push(param)
  },
  [types.ADD_SELF_ASK] (state, param) {
    state.self.askIds.push(param)
  },
  [types.SET_QS] (state, response) {
    state.qs = response.data.qs
  },
  [types.SET_LOGIN_STATE] (state, param) {
    state.isLogedIn = param
  },
  [types.DELETE_Q_BY_KEY] (state, param) {
    delete state.qs[param]
    state.qs = state.qs
  }
}
